# Maintainer: Andreas Radke <andyrtr@archlinux.org>

pkgbase=mesa-demos
pkgname=('mesa-demos' 'mesa-utils')
pkgver=8.4.0
pkgrel=8
arch=('x86_64')
url="https://www.mesa3d.org/"
license=('custom')
makedepends=('mesa' 'freetype2' 'glew' 'freeglut')
options=('debug')
source=(https://mesa.freedesktop.org/archive/demos/mesa-demos-${pkgver}.tar.bz2{,.sig}
        'LICENSE')
sha512sums=('b72d03cad36e0535ff18dcfb222ec4200064b9264f6da51a6e5f03b0dd912abe188bc1d600b6698de3ce6f63b28d2ce01565886ca8e7079edc4967fbf2fb0957'
            'SKIP'
            '25da77914dded10c1f432ebcbf29941124138824ceecaf1367b3deedafaecabc082d463abcfa3d15abff59f177491472b505bcb5ba0c4a51bb6b93b4721a23c2')
validpgpkeys=('E390B9700582FAEA959ACAD41EEF53D38A3A9C67') # "Andreas Boll <andreas.boll.dev@gmail.com>"

build() {
  cd mesa-demos-${pkgver}
  ./configure --prefix=/usr \
    --disable-gles1 \
    --with-system-data-files=/usr/share/mesa-demos

  make
}

package_mesa-demos() {
  pkgdesc="Mesa demos"
  depends=('libgl' 'glew' 'freeglut')
  install=mesa-demos.install

  cd mesa-demos-${pkgver}
  make DESTDIR="${pkgdir}" install

  # add missing egl files
  install -m 0755 src/egl/opengl/{eglgears_wayland,eglgears_x11,eglkms,egltri_wayland,egltri_x11,peglgears,xeglgears,xeglthreads} "${pkgdir}/usr/bin/"

  # remove utils
  rm "${pkgdir}"/usr/bin/{glxinfo,glxgears,eglinfo}

  install -Dm 0644 ../LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}/"
}

package_mesa-utils() {
  pkgdesc="Essential Mesa utilities"
  depends=('libgl')
  provides=('glxinfo' 'glxgears' 'eglinfo')

  cd mesa-demos-${pkgver}
  install -Dm 0755 src/egl/opengl/eglinfo -t "${pkgdir}/usr/bin/"
  install -Dm 0755 src/xdemos/{glxinfo,glxgears} -t "${pkgdir}/usr/bin/"

  install -Dm 0644 ../LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}/"
}
